<?php
class Payments extends TSH_Model
{
  static $_TableName = "payments";
  static $_ItemName = "payment";
  public $total_records = 0;
  public $per_page = 5;

  public function __construct ()
  {
    $this->RecordCount ();
  }

  public function RecordCount ($where = "1=1")
  {
    $sql = "SELECT COUNT(*) as total FROM " . static::TableName() . " WHERE " . $where;
    $result = TSH_Db::Get()->select($sql, false);
    $this->total_records = $result[0]["total"];
  }

  public function NoOfPages ()
  {
    return ceil ($this->total_records / $this->per_page);
  }

  public function GetPage ($page_no, $where = "")
  {
    $result = $this->FindPage ($page_no, $this->per_page, $where, array());
    if ($where != "") $this->RecordCount ($where);
    return $result;
  }

  public static function Rating ($no)
  {
    $blue = $no;
    $white = 5 - $blue;
    $val = "";
    for($i = 0; $i < $blue; $i++)
      $val .= '<span class="rating active"></span>';
    for($i = 0; $i < $white; $i++)
      $val .= '<span class="rating"></span>';
    return $val;
  }
}
?>
