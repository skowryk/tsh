<?php
error_reporting (E_ALL);
/**
 * Basic front controller
 */

// Some handy definitions
define('ROOT', dirname(__FILE__) . '/');
define('CONFIG', ROOT . '/config/');
define('LIB', ROOT . '/lib/');

// Include configurations
include(CONFIG . 'database.php');

// Include base TSH classes
include(LIB . 'TSH.php');

// Include your classes here however you wish
// eg: include(LIB . 'Local/MyClass.php');

// Main - over to you! :-)
include(LIB . 'Local/Tsh.php');

$m = new Payments ();

if (!isset ($_GET["page"]))
  $page = 1;
else
  $page = intval ($_GET["page"]);

$where = "1 = 1 ";
$url_query = "";

if (isset ($_GET["payment_supplier"])) {
  $where .= "AND payment_supplier LIKE '%" . addslashes ($_GET['payment_supplier']) . "%'";
  $url_query .= "&payment_supplier=".$_GET["payment_supplier"];
}
if (isset ($_GET["payment_cost_rating"])) {
  $where .= 'AND payment_cost_rating = ' . intval ($_GET["payment_cost_rating"]);
  $url_query .= "&payment_cost_rating=".$_GET["payment_cost_rating"];
}
$page_data = $m->GetPage($page, $where);

?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>TSH Test Task</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="dist/css/main.css">
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <section class="wrapper">
          <div class="inner-wrapper">
            <div class="heading">
              <h1>Where your money goes</h1>
              <h2>Payments made by Chichester District Council to individual suppliers with a value over £500 made within October.</h2>
            </div>
            <div class="filters">
              <p>
                <input type="text" id="query" placeholder="Search Suppliers">
              </p>
              <p>
                <select id="rate">
                  <option value="" disabled="disabled" selected="selected">Select pounding rate</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
              </p>
              <p>
                <button id="reset">Reset</button>
                <button id="submit">Submit</button>
              </p>
            </div>
            <div class="results">
              <div class="headings">
                <p>Supplier</p>
                <p>Pound Rating</p>
                <p>Reference</p>
                <p>Value</p>
              </div>
              <?php
              foreach ($page_data as $d) {
              ?>
              <div class="row">
                <p><?php echo $d->payment_supplier; ?></p>
                <p><?php echo Payments::Rating($d->payment_cost_rating);?></p>
                <p><?php echo $d->payment_ref; ?></p>
                <p>&pound;<?php echo $d->payment_amount; ?></p>
              </div>
              <?php } ?>
          </div>
          <div class="pagination">
            <a href="?page=<?php if ($page <= 1) echo $page; else echo $page - 1; echo $url_query; ?>"><p class="page prev">&lt;</p></a>
            <?php
            $total = $m->NoOfPages ();
            for ($i = 1; $i <= $total; $i ++)
            {
              $active = $page == $i ? "active" : "";
              echo '<a href="?page='.$i.$url_query.'"><p class="page prev '.$active.'">'.$i.'</p></a> ';
            } ?>
            <a href="?page=<?php if ($page >= $total) echo $page; else echo $page + 1; echo $url_query; ?>"><p class="page next">&gt;</p></a>
          </div>
        </div>
      </section>
      <script src="dist/js/main.js"></script>
    </body>
</html>
