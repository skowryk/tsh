$(document).ready(function () {
  $("#reset").on("click", function () {
    window.location.replace("/index.php");
  });
  $("#submit").on("click", function () {
    var supp = $("#query").val ();
    var rate = $("#rate").val ();
    var str = "";
    if (supp !== "")
      str += "payment_supplier=" + supp;
    if (rate !== "" && rate !== null)
    {
      if (str !== "")
        str += "&";
      str += "payment_cost_rating=" + rate;
    }

    window.location.replace("/index.php?" + str);
  });
});
